package com.example;

import io.jbotsim.core.Topology;
import io.jbotsim.ui.JViewer;

public class Main {

    public static void main(String[] args) {
        Topology topology = new Topology();

        JViewer jViewer = new JViewer(topology);

        topology.start();
    }
}
